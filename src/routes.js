import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'
// import Project from './components/Project.vue'

// Import Views - Dash
// import DashboardView from './components/views/Dashboard.vue'
import ClientesView from './components/views/Cliente.vue'
import ProjectsView from './components/views/Projects.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import EditProject from './components/views/EditProject.vue'
import DeleteProject from './components/views/DeleteProject.vue'
import CreateProject from './components/views/CreateProject.vue'
import CreatePresale from './components/views/CreatePresale.vue'
import PresaleView from './components/views/Presale.vue'
import DedicationView from './components/views/Dedication.vue'
import MilestoneView from './components/views/Milestones.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'presale',
        alias: '',
        component: PresaleView,
        name: 'Presale',
        meta: {description: 'Cotizaciones'}
      }, {
        path: 'projects',
        component: ProjectsView,
        name: 'Projects',
        meta: {description: 'Proyectos'}
      }, {
        path: 'milestones',
        component: MilestoneView,
        name: 'Milestones',
        meta: {description: 'Hitos'}
      }, {
        path: 'clientes',
        component: ClientesView,
        name: 'Clientes',
        meta: {description: 'Clientes'}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'dedications',
        component: DedicationView,
        name: 'Dedication',
        meta: {description: 'Dedicaciones de recursos'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      },
      {
        path: 'projects/edit/:projectid',
        component: EditProject,
        name: 'EditProject'
      },
      {
        path: 'projects/delete/:projectid',
        component: DeleteProject,
        name: 'DeleteProject'
      },
      {
        path: 'projects/create',
        component: CreateProject,
        name: 'CreateProject'
      },
      {
        path: 'presale/create',
        component: CreatePresale,
        name: 'CreatePresale'
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
